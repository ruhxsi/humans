using HumanViewModell;
using System;
using System.Windows;
using System.Windows.Controls;

namespace HumansView
{
    /// <summary>
    /// Für die Information des UI, dass sich ein Element der ObservableCollection geändert hat, s. 
    /// https://stackoverflow.com/questions/8490533/notify-observablecollection-when-item-changes
    /// </summary>
    public partial class MainWindow : Window
    {
        HumanVM human = new HumanVM("Männle", HumanModell.Gender.Male, new Point(100, 100));

        public HumanListVM HumanList { get; set; } = new HumanListVM();

        readonly Random rnd = new Random();
        readonly double humanWidth = 50;

        public MainWindow()
        {
            InitializeComponent();

            lbox.DataContext = HumanList;
            HumanList.Add(human);
            lefttopcorner.DataContext = canvas.DataContext = human;
        }

        private void BtAdd_Click(object sender, RoutedEventArgs e)
        {
            HumanModell.Gender gender = HumanModell.Gender.Transgender;

            if (rbFemale.IsChecked.Value)
            {
                gender = HumanModell.Gender.Female;
            } 
            else if (rbMale.IsChecked.Value)
            {
                gender = HumanModell.Gender.Male;
            }

            human = new HumanVM() { Name = txtName.Text, Gender=gender };
            human.Center = new Point(rnd.NextDouble() * (canvas.ActualWidth - humanWidth),
                rnd.NextDouble() * (canvas.ActualHeight - humanWidth));
            HumanList.Add(human);
        }

        private void lbox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (lbox.SelectedIndex == -1) return;

            human = HumanList.Items[lbox.SelectedIndex];
            canvas.DataContext = human;
        }

        private void BtRemove_Click(object sender, RoutedEventArgs e)
        {
            HumanList.RemoveAt(0);
            lbox.SelectedIndex = 0;
            human = HumanList.Items[0];
        }
    }
}
