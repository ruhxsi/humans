﻿using System.ComponentModel;
using System.Windows;
using System.Windows.Media;

namespace HumanViewModell
{
    public class HumanVM : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        void OnPropertyChanged(string propertyname)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyname));
        }

        private HumanModell.Human human;
        public HumanModell.Human Model {  get { return human; } }

        public string Name
        {
            get { return human.Name; }
            set
            {
                human.Name = value;
                OnPropertyChanged("Name");
            }
        }

        public Brush Color
        {
            get
            {
                switch (human.Gender)
                {
                    case HumanModell.Gender.Male:
                        return Brushes.Blue;
                    case HumanModell.Gender.Female:
                        return Brushes.Pink;
                    case HumanModell.Gender.Transgender:
                        return Brushes.Green;
                    default:
                        return Brushes.DarkGray;
                }
            }
        }

        public HumanModell.Gender Gender
        {
            get { return human.Gender; }
            set
            {
                human.Gender = value;
            }
        }

        public HumanVM(string name, HumanModell.Gender gender, Point center)
        {
            human = new HumanModell.Human(name, gender, center);
        }

        public HumanVM() : this("n.n.", HumanModell.Gender.Transgender, new Point(22, 22))
        { }

        public Point Center
        {
            get { return human.Center; }
            set { human.Center = value;
                OnPropertyChanged("Center");
                OnPropertyChanged("Tag");
            }
        }

        public double PosX
        {
            get { return Center.X; }
            set
            {
                Center = new Point(value, Center.Y);
            }
        }

        public double PosY
        {
            get { return Center.Y; }
            set
            {
                Center = new Point(Center.X, value);
            }
        }

        public int Diameter
        {
            get { return human.DIAMETER; }
        }

        public string Tag
        {
            get { return ToString(); }
        }

        public override string ToString()
        {
            return Model.ToString();
        }

        public static implicit operator HumanVM(HumanModell.Human model)
        {
            HumanVM h = new HumanVM();
            return new HumanVM() { human = model };
        }
    }
}
