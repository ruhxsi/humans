﻿namespace HumanViewModell
{
    public class HumanListVM
    {
        public HumanModell.HumanList Items { get; private set; } = new HumanModell.HumanList();

        public HumanListVM()
        {
        }

        public void Add(HumanVM item)
        {
            Items.Add(item.Model);
        }

        public void RemoveAt(int index)
        {
            Items.RemoveAt(index);
        }
    }
}