﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HumanModell
{
    public enum Gender
    {
        Male,
        Female,
        Transgender
    }
}
