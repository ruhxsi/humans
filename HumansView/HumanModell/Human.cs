﻿using System.Windows;

namespace HumanModell
{
    public class Human
    {
        public readonly int DIAMETER = 33;
        private string name;
        private Gender gender;
        private Point center;

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public Gender Gender
        {
            get { return gender; }
            set { gender = value; }
        }

        public Point Center
        {
            get { return center; }
            set { center = value; }
        }

        public Human(string name, Gender gender,Point center)
        {
            Name = name;
            Gender = gender;
            Center = center;
        }

        public override string ToString()
        {
            return $"{Name}: {Gender} ({Center.X:0.0}/{Center.Y:0.0})";
        }
    }
}
